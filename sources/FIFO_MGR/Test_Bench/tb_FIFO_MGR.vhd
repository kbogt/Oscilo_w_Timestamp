-- Testbench automatically generated online
-- at http://vhdl.lapinoo.net
-- Generation date : 12.9.2018 14:42:57 GMT

library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_FIFOMGR is
--  generic(fifo_din_width : integer :=32;
--          din_width  : integer :=16;
--          latency    : integer :=3;
--          fifo_depth : integer := 1536
--          );
end tb_FIFOMGR;

architecture tb of tb_FIFOMGR is

  component sinewave is
  port (clk :in  std_logic;
        rst :in  std_logic;
        dataout : out integer range -128 to 127;
        dout : out std_logic_vector(15 downto 0)
        );
  end component;

    component design_1_wrapper
        port (ADC_DIN_0     : in std_logic_vector (15 downto 0 );
              CLK           : in std_logic;
              CTRL_REG_0    : in std_logic_vector (31 downto 0 );
              FIFO_AEMPTY_0 : in std_logic;
              FIFO_AFULL_0  : in std_logic;
              FIFO_CLR_0    : out std_logic;
              FIFO_DATA_0   : out std_logic_vector (31 downto 0 );
              FIFO_EMPTY_0  : in std_logic;
              FIFO_FULL_0   : in std_logic;
              FIFO_WE_0     : out std_logic;
              LT_0          : in std_logic;
              RST_0         : in std_logic;
              TSF_0         : in std_logic_vector (31 downto 0 );
              TSS_0         : in std_logic_vector (31 downto 0 );
              TS_0          : in std_logic);
    end component;

    signal ADC_DIN_0     : std_logic_vector (15 downto 0 );
    signal CLK           : std_logic;
    signal CTRL_REG_0    : std_logic_vector (31 downto 0 );
    signal FIFO_AEMPTY_0 : std_logic;
    signal FIFO_AFULL_0  : std_logic;
    signal FIFO_CLR_0    : std_logic;
    signal FIFO_DATA_0   : std_logic_vector (31 downto 0 );
    signal FIFO_EMPTY_0  : std_logic;
    signal FIFO_FULL_0   : std_logic;
    signal FIFO_WE_0     : std_logic;
    signal LT_0          : std_logic;
    signal RST_0         : std_logic;
    signal TSF_0         : std_logic_vector (31 downto 0 );
    signal TSS_0         : std_logic_vector (31 downto 0 );
    signal TS_0          : std_logic;

    constant TbPeriod : time := 4 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

    signal sine_data : std_logic_vector(15 downto 0);

begin

      sigGen : sinewave
      port map (clk =>clk,
                rst =>rst_0,
                dataout => open,
                dout => sine_data
                );

                dut : design_1_wrapper
                port map (ADC_DIN_0     => ADC_DIN_0,
                          CLK           => CLK,
                          CTRL_REG_0    => CTRL_REG_0,
                          FIFO_AEMPTY_0 => FIFO_AEMPTY_0,
                          FIFO_AFULL_0  => FIFO_AFULL_0,
                          FIFO_CLR_0    => FIFO_CLR_0,
                          FIFO_DATA_0   => FIFO_DATA_0,
                          FIFO_EMPTY_0  => FIFO_EMPTY_0,
                          FIFO_FULL_0   => FIFO_FULL_0,
                          FIFO_WE_0     => FIFO_WE_0,
                          LT_0          => LT_0,
                          RST_0         => RST_0,
                          TSF_0         => TSF_0,
                          TSS_0         => TSS_0,
                          TS_0          => TS_0);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;
    ADC_DIN_0<=sine_data;
    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        -- EN <= '0';
        -- CL <= '0';
        -- SB <= (others => '0');
        -- SA <= (others => '0');
        CTRL_REG_0 <= (others => '0'); --EN, CL, SB, SA =0
        TS_0 <= '0';
        LT_0 <= '0';
        TSS_0 <= (others => '0');
        TSF_0 <= (others => '0');

        FIFO_AFULL_0 <= '0';
        FIFO_FULL_0 <= '0';
        FIFO_AEMPTY_0 <= '0';
        FIFO_EMPTY_0 <= '0';

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST_0 <= '0';
        wait for 14 ns;
        RST_0 <= '1';
        wait for 12 ns;

        CTRL_REG_0(31 DOWNTO 24)<=x"02"; --SB
        CTRL_REG_0(23 DOWNTO 16)<=x"04"; --SA

        TSS_0<=x"12345678";
        TSF_0<=x"87654321";
        TS_0<='1';
        wait for 4 ns;
        TS_0<='0';

        wait for 16 ns;
        CTRL_REG_0(0)<='1'; --EN
        CTRL_REG_0(1)<='0'; --CL
        WAIT FOR 16 ns;
        TS_0<='1';
        WAIT FOR 4 ns;
        TS_0<='0';
        LT_0<='0';
        WAIT FOR 160 ns;
        CTRL_REG_0(31 DOWNTO 24)<=x"05"; --SB
        CTRL_REG_0(23 DOWNTO 16)<=x"10"; --SA
        TSS_0<=x"11145678";
        TSF_0<=x"87654444";
        TS_0<='1';
        LT_0<='1';
        WAIT FOR 4 ns;
        TS_0<='0';
        LT_0<='0';
        

        -- EDIT Add stimuli here
--        wait for 100 * TbPeriod;

--        -- Stop the clock and hence terminate the simulation
--        TbSimEnded <= '1';
        wait;
    end process;

end tb;
