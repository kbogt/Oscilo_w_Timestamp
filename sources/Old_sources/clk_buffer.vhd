----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 08/14/2018 12:17:14 PM
-- Design Name: 
-- Module Name: clk_buffer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity clk_buf is
  Port (O  :out STD_LOGIC;
        I :in  STD_LOGIC;
        IB  :in STD_LOGIC);
end clk_buf;

architecture Behavioral of clk_buf is


begin

IBUFGDS_inst : IBUFGDS
-- Edit the following generic to specify the I/O standard for this port.
 generic map (
 IOSTANDARD => "LVDS_25")
 port map (
 O => O, -- Clock buffer output
 I => I, -- Diff_p clock buffer input (connect to top-level port)
 IB => IB -- Diff_n clock buffer input (connect to top-level port)
  );
 -- End of IBUFGDS_inst instantiation



end Behavioral;
