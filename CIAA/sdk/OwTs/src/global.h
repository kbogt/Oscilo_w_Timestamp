/*
 * global.h
 *
 *  Created on: Jan 16, 2018
 *      Author: infolab
 */

#ifndef SRC_GLOBAL_H_
#define SRC_GLOBAL_H_

#define ENABLE_SENSORS
#define ENABLE_DAC
#define ENABLE_RTCLK

#endif /* SRC_GLOBAL_H_ */
